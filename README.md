# MAGATT pipeline
Marker Assisted Gene Annotation Transfert for Triticeae.  
Snakemake pipeline used to transfert GFF annotation on a new assembly with a fine target mapping approach.  

Documentation : [https://umr-gdec.pages.mia.inra.fr/magatt/](https://umr-gdec.pages.mia.inra.fr/magatt/)

Test data are available for download at https://doi.org/10.57745/HHN3G5

