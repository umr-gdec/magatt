FROM mambaorg/micromamba:0.23.0
#FROM continuumio/miniconda3

COPY --chown=micromamba:micromamba envs/magatt.yaml /tmp/env.yaml
RUN micromamba install -y -n base -f /tmp/env.yaml && \
    micromamba clean --all --yes

# install packages
# RUN conda config --add channels conda-forge && \
#     conda config --add channels bioconda && \
#     #conda install -y -n base mamba && \
#     conda env update -n base -f=./env.yml --prune && \
#     conda clean --all --yes && \
#     rm -rf /var/lib/apt/lists/*

# ENV PATH "$CONDA_PREFIX/bin:$PATH"
ARG MAMBA_DOCKERFILE_ACTIVATE=1
ENTRYPOINT ["/usr/local/bin/_entrypoint.sh"]
ENV PATH "$MAMBA_ROOT_PREFIX/bin:$PATH"