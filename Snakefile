configfile: "config.yaml"
container: "docker://helrim/magatt:latest"

rule all: 
	input:
		config['finalPrefix']+'_HC.gff3',
		config['finalPrefix']+'_LC.gff3',
		config['finalPrefix']+'_missing.txt',
		config['finalPrefix']+'_HC.transcripts.fasta',
		config['finalPrefix']+'_HC.cds.fasta',
		config['finalPrefix']+'_HC.cds.valid.explained.txt',
		config['finalPrefix']+'_HC.proteins.fasta',
		config['finalPrefix']+'_LC.transcripts.fasta',
		config['finalPrefix']+'_LC.cds.fasta',
		config['finalPrefix']+'_LC.cds.valid.explained.txt',
		config['finalPrefix']+'_LC.proteins.fasta',
		config['finalPrefix']+'_gmap_differentChrom.txt',
		config['finalPrefix']+'_anchoringSummary.csv',
		config['finalPrefix']+'_blatSummary.csv',
		'report/dag.dot',
		'report/rulegraph.dot'

rule createDiagrams:
	message: "Create dag and rulegraph of the pipeline"
	output: dag='report/dag.dot', rulegraph='report/rulegraph.dot'
	shell:
		"""
		snakemake --dag > {output.dag}
		snakemake --rulegraph > {output.rulegraph}
		"""

include: "rules/preprocessISBP.smk"
include: "rules/preprocessGenomes.smk"
include: "rules/bedtoolsClosest.smk"
include: "rules/geneAnchoring.smk"
include: "rules/checkCds.smk"
