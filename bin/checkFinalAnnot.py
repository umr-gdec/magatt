#!/usr/bin/env python3
# coding: utf-8
import os.path
from os import path
from pprint import pprint
from collections import defaultdict
import argparse
import sys
import re

class checkFinalAnnot (object):
	def __init__(self):
		"""
		Global var
		"""
		# arrays wiht gene ids
		self.refAnnot=[]
		self.targetAnnot=[]
		self.missing=[]
		self.numMissing=0

	def main(self):

		self.getOptions()
		self.checkInputs()
		self.outputFH=open(self.options.missing, 'w')

		# save into arrays the IDs of reference and anchored genes
		self.refAnnot = self.getIds(file=self.options.annot, format='BED')
		self.targetAnnot = self.getIds(file=self.options.input, format='GFF3')
		#self.addGmapResults()

		# get gene ids of missing genes
		self.missing=self.checkMissing()
		sys.stdout.write(" Found {} missing genes in final annot\n".format(self.numMissing))
		self.outputFH.write("\n".join(self.missing)+"\n")
		self.outputFH.close()

	def checkMissing(self):
		missing=[]
		for gene in self.refAnnot:
			if gene not in self.targetAnnot:
				pprint(" Gene {} is not anchored\n".format(gene))
				self.numMissing+=1
				missing.append(gene)
		return missing

	def addGmapResults(self):
		numGenesNewGmap=0
		with open(self.options.gmap) as record:
			for line in record.readlines():
				if not line.startswith('#'):
					currentFeature=self.getGffFeatureType(gffrecord=line)
					if currentFeature == 'gene':
						geneId=self.getFeatureAttribute(attribute='ID', gffrecord=line)
						geneId=re.sub(r'\.\d+\.path1', '', geneId)
						if geneId not in self.targetAnnot:
							self.targetAnnot.append(geneId)
							pprint(" added gmap gene {}".format(geneId))
							numGenesNewGmap+=1
		sys.stdout.write(" {} genes added from GMAP only results\n".format(numGenesNewGmap))

	def getIds(self, file, format):
		sys.stderr.write(" Saving IDs of features in file {} in format {}\n".format(file, format))
		array_ids=[]

		with open(file) as record:
			for line in record.readlines():
				if not line.startswith('#'):

					if format == 'GFF3':
						currentFeature=self.getGffFeatureType(gffrecord=line)
						if currentFeature == 'gene':
							geneId=self.getFeatureAttribute(attribute='ID', gffrecord=line)
							geneId=re.sub(r'\.\d+\.path1', '', geneId)
							array_ids.append(geneId)
						else:
							continue
					
					elif format == 'BED':
						array_ids.append(self.getBedID(bedrecord=line))

					else:
						sys.stderr.write(" Format Unknown")
						sys.exit()

		numids = len(array_ids)
		pprint(" Found {} features saved into array for file {}\n".format(numids, file))
		return array_ids

	def getBedID(self, bedrecord):
		return bedrecord.rstrip('\n').split('\t')[3]
	
	def getGffFeatureType(self, gffrecord):
		return gffrecord.rstrip('\n').split('\t')[2]
	
	def getFeatureAttribute(self, attribute, gffrecord):
		attribute_dict=defaultdict()
		attribute_array=gffrecord.rstrip('\n').split('\t')[8].split(';')
		for attribute_pair in attribute_array:
			(key, val) = attribute_pair.split('=')
			attribute_dict[key] = val
		
		if attribute in attribute_dict.keys():
			return(attribute_dict[attribute])
		else:
			sys.stderr.write(" Cannot find attribute {} in gff record {}".format(attribute, gffrecord))
			sys.exit()

	def getOptions(self):
		parser=argparse.ArgumentParser(description='Check If some genes are missing in the final annotation')
		parser.add_argument('-i', '--input', help='Input GFF3 file to check', required=True)
		#parser.add_argument('-g', '--gmap', help='Input GFF3 file from GMAP only results', required=True)
		parser.add_argument('-a', '--annot', help='Initial Annotation BED file used in the pipeline (BED)', required=True)
		parser.add_argument('-m', '--missing', help='Text file of gene IDs missing in the fina Anotation', required=True)
		self.options=parser.parse_args()

	def checkInputs(self):
		"""
		check for input files and output directory
		"""
		sys.stdout.write(' Check input files:')
		self.checkFile(file=self.options.input)
		self.checkFile(file=self.options.annot)
		#self.checkFile(file=self.options.gmap)

	def checkFile (self,file):
		if os.path.isfile(file):
			sys.stdout.write(" File %s found\n" % str(file))
		else:
			sys.stdout.write(" ERROR: Cannot find file %s \n" % str(file))
			sys.exit()

if __name__== "__main__":

	run=checkFinalAnnot()
	run.main()
