#!/usr/bin/env python3.5
# coding: utf-8
import os.path
from os import path
import collections
from pprint import pprint
import argparse
import sys
import re

class convertGmap (object):
	def __init__(self):
		"""
		Global var
		"""
		# key is mrna id
		self.gffContent = collections.OrderedDict()
		self.mrnaid = ''
		self.newGff = []
		self.knownGenes= []
		self.cdsStatus=collections.defaultdict()
		self.regexlcl=re.compile('lcl\|')

	def main(self):

		self.getOptions()
		self.checkInputs()

		# read gmap gff file and save into dict
		self.openOutputGffHandler()
		self.openGmapGFF()

	def setNewCoord(self,position, offset):
		return int(offset) + int(position)

	def openGmapGFF(self):
		lastMrnaSeen=''
		numGenes=0
		numMrna=0

		featureindex={'CDS':1, 'exon':1}
		
		with open(self.options.gff3) as gffRecords:
			for line in gffRecords.readlines():
				if not line.startswith('#'):
					# get feature type from GFF file
					featureType = self.getGffFeature(gff=line)
					gffArray=line.rstrip('\n').split('\t')

					# get chrom start stop from target name
					newChrom=gffArray[0].split('_')[1]
					offset=gffArray[0].split('_')[2]

					# recalc the coordinates
					newstart=self.setNewCoord(position=gffArray[3], offset=offset)
					newend=self.setNewCoord(position=gffArray[4], offset=offset)

					# check the cds status based on source (path to target.gff)
					cdsStatus=self.checkCds(input=gffArray[1])
						  
					# set the new gffLine
					newGffArray=[newChrom, gffArray[0]] + gffArray[2   :3] +[newstart, newend] + gffArray[5:]
					if featureType == 'mRNA':
						mrnaID = self.getFeatureAttribute(gff=line, attribute='Name')
						newGffArray[8]=self.setGffAttributes(attributes=newGffArray[8], new={'ID':mrnaID,'Parent':mrnaID.split('.')[0], 'Name':mrnaID, 'mapping':self.options.mapping, 'cds':cdsStatus} )
						lastMrnaSeen = mrnaID

						self.gffContent[mrnaID] = [line]
						numMrna+=1

						#reset the index of subfeatures
						featureindex={'CDS':1, 'exon':1}

					elif featureType == 'gene':
						geneId=self.getFeatureAttribute(gff=line, attribute='Name').split('.')[0]
						newGffArray[8]=self.setGffAttributes(attributes=newGffArray[8], new={'ID':geneId, 'Name':geneId, 'mapping':self.options.mapping, 'cds':cdsStatus} )
						if geneId not in self.knownGenes:
							self.knownGenes.append(geneId)
							numGenes+=1
						else:
							continue

					elif featureType in ['CDS', 'exon']:
						featureID=lastMrnaSeen+'.'+featureType.lower()+str(featureindex[featureType])
						Parent=lastMrnaSeen
						newGffArray[8]=self.setGffAttributes(attributes=newGffArray[8], new={'ID':featureID, 'Parent':Parent , 'Name':featureID} )
						featureindex[featureType]+=1
					
					else:
						self.gffContent[lastMrnaSeen].append(line)
					print('\t'.join(map(str, newGffArray)), file=self.outputGffFH)
		sys.stderr.write(' Total Genes anchored: {}\n'.format(numGenes))
		sys.stderr.write(' Total mRNA anchored: {}\n'.format(numMrna))

		
	def checkCds(self, input):
		dir_path = os.path.dirname(input)
		if os.path.isfile(dir_path+'/CDS_OK'):
			return 'CDS_OK'
		elif os.path.isfile(dir_path+'/CDS_CHANGED'):
			return 'CDS_CHANGED'
		else:
			sys.stderr.write(" Cannot guess CDS status for sequence in {}: set cds attribute to UNKNOWN\n".format(dir_path))
			return 'UNKNOWN'

	def setGffAttributes(self, attributes, new):
		attr_array=attributes.rstrip('\n').split(';')
		attr_dict=collections.defaultdict()
		ordered_keys=[]
		for attr in attr_array:
			(key, val) = attr.split('=')
			attr_dict[key] = val
			ordered_keys.append(key)
		
		for newAttr in new.keys():
			attr_dict[newAttr] = new[newAttr]
			if newAttr not in ordered_keys:
				ordered_keys.append(newAttr)

		output_array=[]
		for attr in ordered_keys:
			output_array.append(str(attr)+'='+str(attr_dict[attr]))
		
		return ';'.join(map(str, output_array))

	def getGffFeature(self,gff):
		return gff.rstrip('\n').split('\t')[2]

	def getFeatureAttribute(self,gff,attribute):
		attributes = gff.rstrip('\n').split('\t')[8].split(';')
		dict = collections.defaultdict()

		for info in attributes:
			(key,value) = info.split('=')
			dict[key] = value
		
		if (dict[attribute]):
			if self.regexlcl.search(dict[attribute]) and attribute in ['ID', 'Parent', 'Name']:
				dict[attribute] = self.regexlcl.sub('', dict[attribute])
			return dict[attribute]
		else:
			sys.stderr.write(' ERROR parsing attributes of GFF file at line {}'.format(gff))
			sys.stderr.write(' Cannot find attribute with key {}'.format(attribute))
			return False

	def openOutputGffHandler(self):
		self.outputGffFH=open(self.options.output, 'w')
		print('##gff-version 3', file=self.outputGffFH)

	def getOptions(self):
		parser=argparse.ArgumentParser(description='Recalculate coordinates of a GMAP gff3 (format gene -f2) on a new reference')
		parser.add_argument('-G', '--gff3', help='GMAP result in GFF3 format (-f 2 in gmap)', required=True)
		# parser.add_argument('-r', '--refChrom', help='new reference chromosome of the new target genome', required=True)
		# parser.add_argument('-s', '--start', help='start offset of the new target genome', required=True)
		# parser.add_argument('-e', '--end', help='end offset of the new target genome', required=True)
		# parser.add_argument('-g', '--geneid', help='Gene ID to anchor', required=True)
		# parser.add_argument('-c', '--cds', help='CDS status. This will be added in the final GFF attributes', required=True)
		parser.add_argument('-m', '--mapping', help='Mapping status. This will be added in the final GFF attributes', required=True)
		parser.add_argument('-o', '--output', help='Ouput GFF3', required=True)
		self.options=parser.parse_args()

	def checkFile (self,file):
		if os.path.isfile(file):
			sys.stdout.write(" File %s found\n" % str(file))
		else:
			sys.stdout.write(" ERROR: Cannot find file %s \n" % str(file))
			sys.exit()

	def checkInputs(self):
		"""
		check for input files and output directory
		"""
		sys.stdout.write(' Check input files:')
		self.checkFile(file=self.options.gff3)

if __name__== "__main__":

	run=convertGmap()
	run.main()
