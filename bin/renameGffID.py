#!/usr/bin/env python3
# coding: utf-8
import os.path
from os import path
from pprint import pprint
from collections import defaultdict
import argparse
import sys
import re

class renameIDs (object):
	def __init__(self):
		"""
		Global var
		"""
		self.chromosomeMap=defaultdict()
		self.geneMapCoord=defaultdict()
		self.newGeneMapCoord=defaultdict()
		self.correspondance=defaultdict()
		self.overlappingGenes=defaultdict()

	def main(self):
		self.getOptions()
		self.checkInputs()

		# load inputs
		#self.loadChromosomeMap()

		# open filehandler for output files
		self.prepareOutputFiles()

		# compile regex to check for LC genes
		self.regexLC=re.compile('\d+LC')

		# now generate the new gene IDs and all other stuff
		self.createCoordGeneMap()
		self.createNewMappingIDs()
		self.renameFeatures()

		# close all file handlers
		self.outputGFFFH.close()
		self.outputGFFFHLC.close()


	def renameFeatures(self):
		sys.stdout.write(' Now writing output Gff file {}\n'.format(self.options.output))
		with open(self.options.input) as gffRecord:
			newGeneId=''
			mrnaId=''
			mrnaIndex=0
			subfeaturesIndex ={'CDS':0, 'exon':0, 'utr5p':0, 'utr3p':0, 'ncss5p':0}
			for line in gffRecord.readlines():
				if not line.startswith('#'):
					featureType=line.rstrip('\n').split('\t')[2]
					chrom=line.rstrip('\n').split('\t')[0]

					if featureType == 'gene':
						geneId=self.getFeatureAttribute(gff=line, attribute='ID')
						newGeneId=self.correspondance[geneId]
						# reset mrnaID
						mrnaId=''
						mrnaIndex=0
						#print(" old gene {} changed to {}\n".format(geneId, newGeneId))

						# newGffLine is an array, easier to print
						newGffLine=line.rstrip('\n').split('\t')

						# dict of the attributes to update in the GFF record
						newAttributeDict = {'ID': newGeneId, 'Name': newGeneId, 'previous_id': geneId}
						newGffLine=self.setGffAttribute(dict=newAttributeDict, gff=line)
						newGffLine[1]=self.options.source

						#pprint("New gff record for gene {}".format(newGffLine))

						## print gff output
						if self.regexLC.search(newGeneId):
							print('###', file=self.outputGFFFHLC)
							print('\t'.join(map(str, newGffLine)), file=self.outputGFFFHLC)
						else:
							print('###', file=self.outputGFFFH)
							print('\t'.join(map(str, newGffLine)), file=self.outputGFFFH)


					elif featureType == 'mRNA':
						oldId=self.getFeatureAttribute(gff=line, attribute='ID')
						mrnaIndex+=1
						mrnaId=newGeneId+'.'+str(mrnaIndex)
						subfeaturesIndex ={'CDS':0, 'exon':0, 'utr5p':0, 'utr3p':0, 'ncss5p':0}

						#print(" old mrna {} changed to {}\n".format(oldId, mrnaId))

						# newGffLine is an array, easier to print
						newGffLine=line.rstrip('\n').split('\t')

						# dict of the attributes to update in the GFF record
						newAttributeDict = {'ID': mrnaId, 'Name': mrnaId, 'previous_id': oldId, 'Parent':newGeneId }
						newGffLine=self.setGffAttribute(dict=newAttributeDict, gff=line)
						newGffLine[1]=self.options.source

						#pprint("New gff record for mrna {}".format(newGffLine))
						if self.regexLC.search(mrnaId):
							#print('###', file=self.outputGFFFHLC)
							print('\t'.join(map(str, newGffLine)), file=self.outputGFFFHLC)
						else:
							#print('###', file=self.outputGFFFH)
							print('\t'.join(map(str, newGffLine)), file=self.outputGFFFH)

					elif featureType in ['CDS', 'exon', 'five_prime_UTR', 'three_prime_UTR', 'non_canonical_five_prime_splice_site']:
						oldId=self.getFeatureAttribute(gff=line, attribute='ID')
						# shorten too long feature type for alias in ID
						if featureType == 'five_prime_UTR':
							featureType='utr5p'
						elif featureType == 'three_prime_UTR':
							featureType = 'utr3p'
						elif featureType == 'non_canonical_five_prime_splice_site':
							featureType = 'ncss5p'

						subfeaturesIndex[featureType]+=1
						newId=mrnaId+'.'+featureType+str(subfeaturesIndex[featureType])
						#print(" old {} {} changed to {}\n".format(featureType, oldId, newId))

						# newGffLine is an array, easier to print
						newGffLine=line.rstrip('\n').split('\t')

						# dict of the attributes to update in the GFF record
						newAttributeDict = {'ID': newId, 'Parent':mrnaId }
						newGffLine=self.setGffAttribute(dict=newAttributeDict, gff=line)
						newGffLine[1]=self.options.source

						#pprint("New gff record for mrna {}".format(newGffLine))
						if self.regexLC.search(mrnaId):
							#print('###', file=self.outputGFFFHLC)
							print('\t'.join(map(str, newGffLine)), file=self.outputGFFFHLC)
						else:
							#print('###', file=self.outputGFFFH)
							print('\t'.join(map(str, newGffLine)), file=self.outputGFFFH)

					else:
						continue

	def setGffAttribute(self, dict, gff):
		gff_array=gff.rstrip('\n').split('\t')
		gff_attributes=gff_array[8].split(';')
		attributes_dict=defaultdict()
		attribute_keys_list = []
		output_array =gff_array[0:8]
		for attr in gff_attributes:
			(key,val) = attr.split('=')
			attributes_dict[key]=val
			attribute_keys_list.append(key)

		# update the desired attribute with the new value
		for attr_to_change in dict.keys():
			
			if attr_to_change not in attribute_keys_list:
				attribute_keys_list.append(attr_to_change)
			if attr_to_change == 'previous_id':
				if 'previous_id' in attributes_dict.keys():
					attributes_dict['previous_id'] = attributes_dict['previous_id']+','+dict[attr_to_change]
				else:
					attributes_dict['previous_id'] = dict[attr_to_change]
			else:
				attributes_dict[attr_to_change] = dict[attr_to_change]

		# return all the new attribute
		newAttrArray =[]
		for attr in attribute_keys_list:
			newAttrArray.append(attr+'='+attributes_dict[attr])

		# create the entire gff record
		output_array.append(';'.join(newAttrArray))
		return output_array

	def createNewMappingIDs(self):
		for chrom in self.geneMapCoord.keys():
			coords = map(int, self.geneMapCoord[chrom].keys())
			geneNum =0
			step=100
			sys.stderr.write(" Working on chromosome {}\n".format(chrom))
			for coord in sorted(coords):
				oldGeneId=self.geneMapCoord[chrom][str(coord)]
				geneNum += step
				newGeneId = self.options.prefix + self.chromosomeMap[chrom] + self.options.version + str(geneNum).zfill(8)
				if self.regexLC.search(oldGeneId):
					newGeneId+='LC'
				#print(" current gene {} at coordinate {} on chrom {}: new ID = {}".format(oldGeneId, coord, chrom, newGeneId))
				self.newGeneMapCoord[chrom][str(coord)]=newGeneId
				self.correspondance[oldGeneId] = newGeneId
			numGenesInChrom=len(self.newGeneMapCoord[chrom].keys())
			sys.stdout.write(" {} new gene IDs on chrom {}\n".format(numGenesInChrom, chrom))
		numGenes = len(self.correspondance.keys())
		sys.stdout.write(" New gene IDs created for {} genes\n".format(numGenes))

		# save correspondance betwenn old and new id to file
		with open(self.options.correspondance, 'w') as tmpfh:
			print("old_id\tnew_ID", file=tmpfh)
			for gene in sorted(self.correspondance.keys()):
				print("{}\t{}".format(gene, self.correspondance[gene]), file=tmpfh)

		# save overlapping gene info to BED file
		with open(self.options.overlap, 'w') as tmpfh:
			for gene in sorted(self.overlappingGenes.keys()):
				newGeneId=self.correspondance[str(gene)]
				bed_array=[self.overlappingGenes[gene]['chrom'], self.overlappingGenes[gene]['start']-1, self.overlappingGenes[gene]['stop'], newGeneId]
				print("\t".join(map(str, bed_array)), file=tmpfh)

	def createCoordGeneMap(self):
		"""
		populate dictionary self.geneMapCoord with structure:
		defaultdict(None,
            {'Chr1A': defaultdict(None,
                                  {'102794': 'TraesCS1A02G000400',
                                   '11085': 'TraesCS1A02G000100LC',
                                   '121232': 'TraesCS1A02G000500LC',
                                   '40098': 'TraesCS1A02G000100',
                                   '42451': 'TraesCS1A02G000200LC',
                                   '70239': 'TraesCS1A02G000200',
                                   '94185': 'TraesCS1A02G000300LC',
                                   '95906': 'TraesCS1A02G000300',
                                   '95926': 'TraesCS1A02G000400LC'}),
             'Chr1B': defaultdict(None, {}),
             'Chr1D': defaultdict(None, {}),
             'Chr2A': defaultdict(None, {}),
             'Chr2B': defaultdict(None, {}),
             'Chr2D': defaultdict(None, {}),
             'Chr3A': defaultdict(None, {}),
             'Chr3B': defaultdict(None, {}),
             'Chr3D': defaultdict(None, {}),
             'Chr4A': defaultdict(None, {}),
             'Chr4B': defaultdict(None, {}),
             'Chr4D': defaultdict(None, {}),
             'Chr5A': defaultdict(None, {}),
             'Chr5B': defaultdict(None, {}),
             'Chr5D': defaultdict(None, {}),
             'Chr6A': defaultdict(None, {}),
             'Chr6B': defaultdict(None, {}),
             'Chr6D': defaultdict(None, {}),
             'Chr7A': defaultdict(None, {}),
             'Chr7B': defaultdict(None, {}),
             'Chr7D': defaultdict(None, {}),
             'ChrUnknown': defaultdict(None, {})})
		"""
		sys.stdout.write('populate dictionary self.geneMapCoord\n')
		numGenesAddedInDict=0
		numGenesWithSamCoord=0
		with open(self.options.input) as gffRecord:
			for line in gffRecord.readlines():
				if not line.startswith('#'):
					featureType=line.rstrip('\n').split('\t')[2]
					chrom=line.rstrip('\n').split('\t')[0]
					coord=line.rstrip('\n').split('\t')[3]
					stop=line.rstrip('\n').split('\t')[4]
					geneId=self.getFeatureAttribute(gff=line, attribute='ID')


					# check if this chromosome is in the mapping dictionaries
					if (chrom not in self.chromosomeMap.keys()):
						self.chromosomeMap[chrom] = chrom
						self.geneMapCoord[chrom] = defaultdict()
						self.newGeneMapCoord[chrom] = defaultdict()
						
					if featureType == 'gene':
						while coord in self.geneMapCoord[chrom].keys():
							sys.stderr.write(" WARNING: gene {} has same coordinate as gene {} \n".format(geneId, self.geneMapCoord[chrom][coord]))
							sys.stderr.write(" WARNING: increase coordinate by 1 in dictionnary\n")
							self.overlappingGenes[geneId] = {'start': int(coord), 'chrom': chrom, 'stop':int(stop)}
							coord = str(int(coord) +1)
							numGenesWithSamCoord+=1

						self.geneMapCoord[chrom][coord] = geneId
						numGenesAddedInDict+=1
		sys.stdout.write(" Added {} in dictionary\n".format(numGenesAddedInDict))
		sys.stdout.write("       {} had same coordinates on chromosomes\n".format(numGenesWithSamCoord))

	def getFeatureAttribute(self, gff, attribute):
		attr_array=gff.rstrip('\n').split('\t')[8].split(';')
		attr_dict=defaultdict()
		for attr in attr_array:
			(key, val) = attr.split('=')
			attr_dict[key] = val
		if attribute in attr_dict.keys():
			return attr_dict[attribute]
		else:
			sys.stderr.write(' ERROR: no attribute {} in gff record {}'.format(attribute, gff))
			sys.exit()

	def prepareOutputFiles(self):
		# for HC genes
		self.outputGFFFH=open(self.options.output, 'w')
		self.outputGFFFH.write("##gff-version 3\n")
		# for LC genes
		self.outputGFFFHLC=open(self.options.lowconf, 'w')
		self.outputGFFFHLC.write("##gff-version 3\n")

	def loadChromosomeMap(self):
		with open(self.options.map) as maprecord:
			for line in maprecord.readlines():
				if not line.startswith('#'):
					(name, id) = line.rstrip('\n').split('\t')
					self.chromosomeMap[id] = name
					self.geneMapCoord[id] = defaultdict()
					self.newGeneMapCoord[id] = defaultdict()
		sys.stdout.write(' found {} chromosome records in file {}\n'.format(len(self.chromosomeMap.keys()),self.options.map ))
		pprint(self.chromosomeMap)

	def getOptions(self):
		parser=argparse.ArgumentParser(description='Check If some genes are missing in the final annotation')
		parser.add_argument('-i', '--input', help='Input GFF3 file to rename ', required=True)
		parser.add_argument('-o', '--output', help='Output GFF3 file', required=True)
		parser.add_argument('-l', '--lowconf', help='Output GFF3 file for Low Confidence genes', required=True)
		parser.add_argument('-p', '--prefix', help='Prefix for gene ID (ex: TraesCS)', required=True)
		parser.add_argument('-v', '--version', help='Version of the new gene set. Will be added into the gene name (ex 03G: TraesCS03G)', required=True)
		parser.add_argument('-s', '--source', help='Source for GFF file', required=True)
		parser.add_argument('-c', '--correspondance', help='Correspondance file of gene IDs (BED)', required=True)
		parser.add_argument('-x', '--overlap', help='File of potentially overlapping genes', required=True)
		parser.add_argument('-m', '--map', help='File of mapping of chromosome ID (two columns, chromosome Name<TAB>Chromosome code', required=True)
		self.options=parser.parse_args()
		sys.stdout.write(" Parameters : {}".format(self.options))

	def checkInputs(self):
		"""
		check for input files and output directory
		"""
		sys.stdout.write(' Check input files:')
		self.checkFile(file=self.options.input)
		self.checkFile(file=self.options.map)

	def checkFile (self,file):
		if os.path.isfile(file):
			sys.stdout.write(" File %s found\n" % str(file))
		else:
			sys.stdout.write(" ERROR: Cannot find file %s \n" % str(file))
			sys.exit()

if __name__== "__main__":

	run=renameIDs()
	run.main()
