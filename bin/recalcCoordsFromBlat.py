#!/usr/bin/env python3.5
# coding: utf-8
import os.path
from os import path
from collections import defaultdict
from collections import OrderedDict
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
from Bio import SeqIO
from pprint import pprint
import argparse
import pysam
import sys

"""
Command line arguments
"""
# blatMapping = sys.argv[1] # Blat result file from microMappingPipeline.py
# summaryMapping = sys.argv[2] # Summary of the blat mapping from microMappingPipeline.py
# outputGff = sys.argv[3] # output gff file ...
# genomeQuery = sys.argv[4] # fasta file of the v1 genome (must be fai indexed)
# genomeTarget = sys.argv[5] # fasta file of the v2 genome (must be fai indexed)
# annotQuery = sys.argv[6] # initial GFF file of the annotation

class recalcCoords (object):
	def __init__(self):
		"""
		Global var
		"""
		self.geneAnnot=OrderedDict()
		self.genomeMap=defaultdict()
		self.chromosomeList=[]
		self.geneMapping=defaultdict()
		self.newCoord=defaultdict()
		self.outputGff=''
		self.geneMapping=defaultdict()
		self.newGeneAnnot=defaultdict()
		self.attributeMapping = ''


	def main(self):

		self.getOptions()
		self.checkInputs()
		self.openFastaHandlers()
		self.openOutputGffHandler()

		# Save blat mapping info into dict. self.geneMapping is a dict with gene id as key
		self.saveBlatResults(summary=self.options.summaryMapping,blat=self.options.blat)

		# Save Annotation information from GFF file. self.geneAnnot is a dict with gene id as key
		self.saveAnnotFromGFF()

		# For each gene, with full match (with or without missmatches), we try to recalc the coordinates
		self.parseAnnotAndRecalc()


	def parseAnnotAndRecalc(self):

		for geneID in sorted(self.geneAnnot.keys()):
			blat=''
			annot=''
			self.newGeneAnnot[geneID]=[]
			#print(" Dealing with gene {}\n".format(geneID))

			# save current anno for the working gene.
			annot=self.geneAnnot[geneID]
			if geneID in self.geneMapping.keys():
				blat=self.geneMapping[geneID]
				#print(' Blast summary found: {}'.format(blat))
				self.attributeMapping = blat['mappingStatus']
			else:
				#sys.stderr.write(" ERROR cannot find gene {} in blat data\n".format(geneID))
				self.attributeMapping = 'unmapped'
				continue

			if self.attributeMapping in ['unmapped', 'imperfectMatch']:
				sys.stderr.write(" refine mapping with gmap in secondary process")

			elif self.attributeMapping in ['fullPerfectMatch', 'fullMatchWithMissmatches']:
				'''
				Recalc the new coord
				'''
				self.attributeMapping = blat['mappingStatus']
				self.newGeneAnnot[geneID]=self.recalcCoord(
					strand=blat['hitStrandOnTarget'],
					queryChrom=blat['queryChrom'],
					queryStart=blat['queryStart'],
					queryStop=blat['queryStop'],
					targetChrom=blat['targetChrom'],
					targetStart=blat['targetStart'],
					targetStop=blat['targetStop'],
					hitStart=blat['hitStartOnTarget'],
					hitStop=blat['hitStopOnTarget'],
					annot=annot,
					blatMapping=blat)

				# print the final gff
				self.printGff(geneID=geneID, annot=self.newGeneAnnot[geneID], outputFH=self.outputGffFH)
			else:
				sys.stderr.write(" ERROR - cannot guess the mapping category {} of the gene {} ".format(self.attributeMapping, geneID))
				sys.exit()

	def printGff(self,geneID, annot, outputFH):
		sys.stdout.write(" writting final GFF output for gene {} with new annot into GFF file {}".format(geneID, annot, outputFH))
		outputFH.write('###'+"\n")
		for gffrecord in annot:
			pprint(" current gff record as array {}".format(gffrecord))
			outputFH.write("\t".join(map(str,gffrecord))+"\n")

	def recalcCoord(self,strand,queryChrom,queryStart,queryStop,targetChrom,targetStart,targetStop,hitStart,hitStop,annot,blatMapping):
		newAnnotCoord=[]
		transfertStatus=0
		geneid=self.getGffAttribute(gff_string = annot[0] , tag = 'ID')
		newCdsSequence=''
		queryCdsSequence=''

		#print(' Hit on strand {} of the target'.format(strand))
		#print(' target chrom {} starting from {} and ending at {}'.format(targetChrom,targetStart,targetStop))

		for feature in annot:
			feature=feature.rstrip('\n').split("\t")
			#print('************ recalc coord for feature {}'.format(feature))
			(newStart,newStop,newFeatureLength,newStrand,newChrom)=(0,0,0,0,0)

			# extract infos from col #9
			gffInfoTable=feature[8].split(';')

			# and transform it into a dict
			gffInfoDict=defaultdict()
			for infoPair in gffInfoTable:
				(key,value)=infoPair.split('=')
				gffInfoDict[key] = value

			# recalc the start and stop of current feature
			if strand == '+':
				#FORMULA: NewCoord = targetStart + hitStart -1 +(oldCoord - geneStart)
				newStart=int(targetStart) + int(hitStart) + (int(feature[3]) - int(queryStart))
				newStop=int(targetStart) + int(hitStart) + (int(feature[4]) - int(queryStart))
				newFeatureLength = newStop - newStart + 1
				# strand stays the same as in query gff
				newStrand=feature[6]
				newChrom=targetChrom

			elif strand == '-':
				#FORMULA: NewCoord = targetStart + hitStart -1 +(geneStop - oldCoord)
				newStart=int(targetStart) + int(hitStart) +1 + (int(queryStop) - int(feature[4]))
				newStop=int(targetStart) + int(hitStart) +1 + (int(queryStop) - int(feature[3]))
				newFeatureLength = newStop - newStart + 1
				# strand opposit from the query
				if feature[6] == '+':
					newStrand='-'
				elif feature[6] == '-':
					newStrand='+'
				newChrom=targetChrom

			else:
				sys.stderr.write(" Cannot parse strand of the hit !")
				exit()


			#print(' New start = {} new stop = {} : new feature length = {}'.format(newStart,newStop,newFeatureLength))

			# save the new coords for mrna and gene features
			if feature[2] in ['gene','mRNA']:
				newFeature = feature
				newFeature[0]=newChrom
				newFeature[3]=newStart
				newFeature[4]=newStop
				newFeature[6]=newStrand
				#print(" new feature coords on target genome: {}".format(newFeature))
				newAnnotCoord.append(newFeature)

			else:

				"""
				CHECK FOR CDS/UTR/EXONS integrity:
				extract the fasta sequence to compare the UTR/EXON/CDS features before and after recalc
				1. from query sequence
				"""
				queryFeatureSeq=self.getFastaSeq(fasta=self.queryFasta,
					chrom=queryChrom,
					start=int(feature[3])-1,
					stop=int(feature[4]))
				querySeqRecord=SeqRecord(
					Seq(queryFeatureSeq, IUPAC.ambiguous_dna),
					id=gffInfoDict['ID'],
					name=gffInfoDict['ID'],
					description='feature ' + feature[2] + ' of sequence extracted from ' + self.options.queryGenome)
				#print(querySeqRecord,len(querySeqRecord))

				"""
				2. From target Sequence
				"""
				targetFeatureSeq=self.getFastaSeq(fasta=self.targetFasta,
					chrom=newChrom,
					start=int(newStart),
					stop=int(newStop))
				targetSeqRecord=SeqRecord(
					Seq(targetFeatureSeq, IUPAC.ambiguous_dna),
					id=gffInfoDict['ID'],
					name=gffInfoDict['ID'],
					description='feature ' + feature[2] + ' of sequence extracted from ' + self.options.targetGenome)

				"""
				Concat the cds sequence if current feature is cds
				"""
				if feature[2] == 'CDS':
					newCdsSequence+=str(targetSeqRecord.seq)
					queryCdsSequence+=str(querySeqRecord.seq)

				"""
				Test if the 2 sequences are the same to validate the recalculation
				"""
				# revComp if needed the query sequence (gff strand = feature[6])
				if feature[6] == '-':
					# need to revComp the Sequence
					seq1=str(querySeqRecord.reverse_complement().seq).upper()
				elif feature[6] == '+':
					seq1=str(querySeqRecord.seq).upper()
				else:
					sys.stderr.write(" Error when converting Query sequence to uppercase for comparison")
					sys.stderr.write(" Cannot guess strand of the Query feature on the query sequence")
					exit()

				if newStrand == '-':
					# need to revComp the Sequence
					seq2=str(targetSeqRecord.reverse_complement().seq).upper()
				elif newStrand == '+':
					seq2=str(targetSeqRecord.seq).upper()
				else:
					sys.stderr.write(" Error when converting Target sequence to uppercase for comparison")
					sys.stderr.write(" Cannot guess strand of the Target feature on the target sequence")
					exit()

				if seq1 != seq2:
					sys.stderr.write(" error with feature {} :".format(gffInfoDict['ID']))
					sys.stderr.write( "the 2 sequences are not the same: warning added into the gff file ! \n" )
					#sys.stderr.write('  Query: {}'.format(seq1))
					#sys.stderr.write(' Target: {}'.format(seq2))
					transfertStatus=1

				else:
					print( "the two sequences are the same for feature {}".format(feature[8].split(';')[0]) )
					#print(" new feature coords on target genome: {}".format(newFeature))

				newFeature = feature
				newFeature[0]=newChrom
				newFeature[3]=newStart
				newFeature[4]=newStop
				newFeature[6]=newStrand
				newAnnotCoord.append(newFeature)


		"""
		If at least one feature failed when recal coords,
		aka the sequences are not the same in target and query,
		we add a tag in the gene/mrna features
		"""
		if (transfertStatus == 1):
			sys.stderr.write(" At least one UTR/EXON/CDS feature failed when recalc coords for gene {}.\n".format(geneid))
			if self.getTranslatedCDS(seq=newCdsSequence,strand=newStrand) != self.getTranslatedCDS(seq=queryCdsSequence,strand=feature[6]):
    				#print("Translated CDS are NOT the same")
				newAnnotCoord = self.addGffTag(annotArray=newAnnotCoord,tag='cds', value='CDS_CHANGED')
			else:
				newAnnotCoord = self.addGffTag(annotArray=newAnnotCoord,tag='cds', value='CDS_OK')
		else:
			newAnnotCoord = self.addGffTag(annotArray=newAnnotCoord,tag='cds', value='CDS_OK')

		"""
		Wa add the mapping status to every gene/mrna features
		"""
		newAnnotCoord = self.addGffTag(annotArray=newAnnotCoord,tag='mapping', value=self.attributeMapping)
		return newAnnotCoord

	def getGffAttribute(self, gff_string, tag):
		gff_attributes = gff_string.rstrip('\n').split('\t')[8].split(';')
		dict = defaultdict()
		for infos in gff_attributes:
			(key, value) = infos.split('=')
			dict[key] = value

		if tag in dict.keys():
			return dict[tag]
		else:
			sys.stderr.write(' ERROR : when tryin to get the tag {} in gff line {}'.format(tag, gff_string))
			sys.stderr.write(' ERROR : No such tag in the gff line')
			return False

	def addGffTag(self, annotArray, tag, value):
		annotFinal=[]
		tagarray=[]
		for featureArray in annotArray:
			# on ajoute les tags que aux faetures mrna et gene
			if featureArray[2] in ['gene', 'mRNA']:
				tagdict=defaultdict()
				tagarray=[]
				attribute_array=featureArray[8].rstrip("\n").split(';')
				for attrib in attribute_array:
					(key, val)=attrib.split('=')
					if key not in tagarray:
						tagarray.append(key)
						tagdict[key]=val
				if tag not in tagarray:
					tagarray.append(tag)

				tagdict[tag]=value
				attribarray=[]
				for attrib in tagarray:
					attribarray.append(str(attrib)+'='+str(tagdict[attrib]))

				featureArray[8]=';'.join(attribarray)
			annotFinal.append(featureArray)
		return annotFinal

	def getTranslatedCDS(self, seq, strand):
		#print(" Evaluating CDS sequence for {}".format(seq))
		seqObject=Seq(seq, IUPAC.ambiguous_dna)

		if (strand == '-'):
			seqObject=seqObject.reverse_complement()
			#print(" Rev Comp sequence is {}".format(seqObject))

		pepObject=seqObject.translate()
		#print(" Translated sequence is {}".format(pepObject))

	def saveAnnotFromGFF(self):
		sys.stdout.write(" 2. saving Query Annotation from file {} into Dict \n".format(self.options.gff))
		sys.stdout.write("===========================================================================\n")
		geneID=''
		with open(self.options.gff) as gffRecords:
			for line in gffRecords.readlines():
				if line.startswith('#'):
					continue

				# transform gff line into array
				(chrom,source,featureType,start,stop,score,strand,frame,info)=line.rstrip('\n').split('\t')
				if chrom not in self.genomeMap.keys():
					self.genomeMap.update({chrom: defaultdict()})

				# save the last column with info tags
				gffInfoTable=info.split(';')

				# and transform it into a dict
				gffInfoDict=defaultdict()
				for infoPair in gffInfoTable:
					(key,value)=infoPair.split('=')
					gffInfoDict[key] = value

				if featureType == 'gene':
					geneID=gffInfoDict['ID']
					self.geneAnnot[geneID] = [line.rstrip('\n')]
					self.genomeMap[chrom][start] = geneID

				else:

					if self.options.mode == 'first':

						# only keep CDS/exons of '.1' transcript
						if featureType in ['CDS', 'exon'] and gffInfoDict['Parent'].split('.')[-1] == '1':
							self.geneAnnot[geneID].append(line.rstrip('\n'))

						# only keep mrna of first transcript
						elif featureType == 'mRNA' and gffInfoDict['ID'].split('.')[-1] == '1':
							self.geneAnnot[geneID].append(line.rstrip('\n'))

					elif self.options.mode == 'all':

						self.geneAnnot[geneID].append(line.rstrip('\n'))

					else:
						sys.stderr.write(' ERROR: problem with mode parameter: value {} unauthorized'.format(self.options.mode))
						exit(1)


		numGenes=len(self.geneAnnot.keys())
		sys.stdout.write(" Found {} genes in GFF file {}\n".format(numGenes,self.options.gff))

	def saveBlatResults(self, summary, blat):
		sys.stdout.write(" 1. saving blat results {} and summary {} into Dict \n".format(blat,summary))
		sys.stdout.write("===========================================================================\n")

		# SUMMARY DATA
		with open(summary) as summaryData:
			for line in summaryData.readlines():
				#sys.stdout.write(' current record is {}'.format(line))

				(geneName,markerPair,mappingStatus,queryChrom,markerStatus,markerPair2,geneName2,geneQueryStart,geneQueryEnd,geneQueryStrand,targetLengthStatus,leftMakerId,leftMakerQueryLoc,leftMarkerTargetLoc,rightMarkerId,rightMakerQueryLoc,rightMarkerTargetLoc) = line.rstrip('\n').split("\t")

				self.geneMapping[geneName] = {
					'geneId': geneName,
					'gffRecords': 'NA',
					'gffSubfeatures': [],
					'targetStart':'NA',
					'targetStop':'NA',
					'targetChrom':'NA',
					'hitStrandOnTarget':'NA',
					'hitStartOnTarget':'NA',
					'hitStopOnTarget':'NA',
					'hitStrandOnTarget':'NA',
					'queryStart':geneQueryStart,
					'queryStop':geneQueryEnd,
					'queryChrom':queryChrom,
					'queryStrand':geneQueryStrand,
					'mappingStatus':mappingStatus
				}
		numGenesInDict=len(self.geneMapping.keys())

		# BLAT MAPPING DATA
		with open(blat) as blatData:
			for line in blatData.readlines():

				(geneName,markerPair,mappingStatus,mappingStatus2,blatCoverage,blatIndelBases,blatMissmatches,hitStartOnTarget,hitStopOnTarget,hitStrand,targetLength,targetName)=line.rstrip('\n').split('\t')

				if geneName in self.geneMapping.keys():

					if mappingStatus == 'unmapped':
						#sys.stdout.write(" gene {} is not mapped: no target information to recover.\n".format(geneName))
						continue
					else:
						(prefix,targetChrom,targetStart,targetStop)=targetName.split('_')
						self.geneMapping[geneName]['targetStart'] = targetStart
						self.geneMapping[geneName]['targetStop'] = targetStop
						self.geneMapping[geneName]['targetChrom'] = targetChrom
						self.geneMapping[geneName]['hitStrandOnTarget'] = hitStrand
						self.geneMapping[geneName]['hitStartOnTarget'] = hitStartOnTarget
						self.geneMapping[geneName]['hitStopOnTarget'] = hitStopOnTarget

				else:
					sys.stderr.write(" ERROR: gene {} found in blat results but not in summary data! \n".format(geneName))
					sys.exit()

		sys.stdout.write(" Number of records in Gene Dict based on summaryData: {}\n".format(numGenesInDict))

	def getFastaSeq(self,fasta,chrom,start,stop):
		seq=fasta.fetch(reference=chrom, start=start,end=stop)
		#print(" fasta sequence for chrom %s from %s to %s\n" %(chrom,start,stop))
		#print(seq)
		return seq

	def checkFile (self,file):
		if os.path.isfile(file):
			sys.stdout.write(" File %s found\n" % str(file))
		else:
			sys.stdout.write(" ERROR: Cannot find file %s \n" % str(file))
			sys.exit()

	def checkDir(self,directory):
		if os.path.isdir(directory):
			sys.stdout.write(" Directory %s found\n" % str(directory))
			return 1
		else:
			sys.stdout.write(" Cannot find directory %s \n" % str(directory))
			return 0

	def getOptions(self):
		parser=argparse.ArgumentParser(description='Recalculate coordinates on a new reference sequence based on BLAT summary mapping')
		parser.add_argument('-b', '--blat', help='Blat result file from microMappingPipeline.py', required=True)
		parser.add_argument('-s', '--summaryMapping', help='Summary of the blat mapping from microMappingPipeline.py', required=True)
		parser.add_argument('-o', '--outputGff', help='Ouput GFF', required=True)
		parser.add_argument('-q', '--queryGenome', help='fasta file of the v1 genome (must be fai indexed)', required=True)
		parser.add_argument('-t', '--targetGenome', help='fasta file of the v2 genome (must be fai indexed)', required=True)
		parser.add_argument('-g', '--gff', help='initial GFF file of the annotation from the v1 genome', required=True)
		parser.add_argument('-m', '--mode', help='Transfer mode: single or all isoforms (possible values: [first, all])', choices={'first', 'all'}, required=True)
		self.options=parser.parse_args()

	def openFastaHandlers(self):
		"""
		open for reading the query and target reference file
		This will be used to extract the dna sequences before and after recalculation to ensure that the sequences are the same
		and that the calculation is correct
		"""
		sys.stdout.write(' Open FASTA files for sequence extraction\n')
		self.queryFasta = pysam.FastaFile(self.options.queryGenome)
		self.targetFasta = pysam.FastaFile(self.options.targetGenome)

	def openOutputGffHandler(self):
		self.outputGffFH=open(self.options.outputGff, 'w')
		self.outputGffFH.write('##gff-version 3')

	def checkInputs(self):
		"""
		check for input files and output directory
		"""
		sys.stdout.write(' Check input files:')
		self.checkFile(file=self.options.blat)
		self.checkFile(file=self.options.summaryMapping)
		self.checkFile(file=self.options.gff)
		self.checkFile(file=self.options.queryGenome)
		self.checkFile(file=self.options.targetGenome)


if __name__== "__main__":

	run=recalcCoords()
	run.main()
