#!/usr/bin/env python3.5

import pysam
import sys

inputBamfile = sys.argv[1]
outputBamFile = sys.argv[2]
#minMapQ=30
minMapQ=int(sys.argv[3])
#maxMismatches=0
maxMismatches=int(sys.argv[4])

numTotalReads = 0
numFilteredReads = 0
numExcludedReads = 0

samfile = pysam.AlignmentFile(inputBamfile, "rb")
filteredOutput = pysam.AlignmentFile(outputBamFile, "wb", template=samfile)
for read in samfile:
	numTotalReads += 1

	mapq=read.mapping_quality
	cigarLength=read.infer_read_length()
	readLength=read.query_length
	tags=read.get_tags()
	numMissmatches=tags[0][1]


	# keep reads with defined mapQ, missmatches and 100% coverage
	if ( mapq >= minMapQ and cigarLength == readLength and numMissmatches <= maxMismatches):
		#sys.stdout.write(" mapping OK\n")
		filteredOutput.write(read)
		numFilteredReads +=1

	else:
		sys.stderr.write(" mapping not ok for read %s: qlength=%s cigarLength=%s mapQ=%s numMissmatches=%s\n" % (str(read.query_name),str(readLength), str(cigarLength), str(mapq), str(numMissmatches)))

		#some log
		# sys.stdout.write (" read: %s\n" % str(read))
		# sys.stdout.write(" mapped read length from cigar: %s\n" % str(cigarLength))
		# sys.stdout.write(" read length: %s\n" % str(readLength))
		# sys.stdout.write(" mapq: %s\n" % str(mapq))
		# sys.stdout.write(" tags: %s\n" % str(tags))
		# sys.stdout.write(" num missmaches: %i\n" % int(numMissmatches))
		numExcludedReads +=1

samfile.close()
filteredOutput.close()

sys.stdout.write(" Found %i reads in Bam file\n" % numTotalReads)
sys.stdout.write(" Keep %i reads in final BAM file\n" % numFilteredReads)
