.. MAGATT documentation master file, created by
   sphinx-quickstart on Thu Jan  4 15:41:34 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MAGATT's documentation!
==================================

Marker Assisted Gene Annotation Transfert for Triticeae.  

Snakemake pipeline used to transfert GFF annotation on a new assembly with a fine target mapping approach.  

GitLab Repository : https://forgemia.inra.fr/umr-gdec/magatt

Test data are available for download at https://doi.org/10.57745/HHN3G5


.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Installation

   source/install


.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Configuration

   source/pipeline_conf
   source/run_pipeline
