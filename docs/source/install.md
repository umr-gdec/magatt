# Dependancies and installation


## Get the pipeline

```console
$ git clone https://forgemia.inra.fr/umr-gdec/magatt.git
```

## Dependencies - Snakemake

* Snakemake : >=5.5.2

All the dependancies are installed in a docker image : https://hub.docker.com/r/helrim/magatt used by default by MAGATT