rule grepGffFeature:
    message: " Collect selected features from GFF file"
    input: config['annotationQuery']
    params: config['featureType']
    output: temp(config['results']+"/1.features.bed")
    log: config['results']+"/1.grepGffFeature.log"
    shell: "bin/gff2bed.sh {params} {input} 1> {output} 2> {log}"

rule splitGffPerChrom:
    message: "Split Gff Features per chromosome: current is {wildcards.chrom}"
    input: config['results']+"/1.features.bed"
    output: temp(config['results']+"/1.features/{chrom}.bed")
    log: config['results']+"/1.features/{chrom}.fgrep.log"
    params: "{chrom}"
    shell:  "fgrep -i {params} {input} 1> {output} 2> {log}"

rule indexQuery:
	message: " Indexing Query fasta file using samtools faidx"
	# container: "docker://helrim/magatt:latest"
	#container: "docker://biocontainers/samtools:v1.9-4-deb_cv1"
	input: config['queryFasta']
	output: config['queryFasta']+'.fai'
	shell: "samtools faidx {input}"

rule indexTarget:
	message: " Indexing Target fasta file using samtools faidx"
	# container: "docker://helrim/magatt:latest"
	#container: "docker://biocontainers/samtools:v1.9-4-deb_cv1"
	input: config['targetFasta']
	output: config['targetFasta']+'.fai'
	shell: "samtools faidx {input}"
