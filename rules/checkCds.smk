rule validateCdsHC:
	message: " check CDS integrity for HC genes"
	container: "docker://helrim/magatt:latest"
	input: config['finalPrefix']+'_HC.cds.fasta'
	output: fasta=config['finalPrefix']+'_HC.cds.valid.fasta',
		csv=config['finalPrefix']+'_HC.cds.valid.explained.txt'
	shell:
		"""
		fastavalidcds -f {input} -e 1> {output.fasta} 2> {output.csv}
		"""

rule validateCdsLC:
	message: " check CDS integrity for LC genes"
	container: "docker://helrim/magatt:latest"
	input: config['finalPrefix']+'_LC.cds.fasta'
	output: fasta=config['finalPrefix']+'_LC.cds.valid.fasta',
		csv=config['finalPrefix']+'_LC.cds.valid.explained.txt'
	shell:
		"""
		fastavalidcds -f {input} -e 1> {output.fasta} 2> {output.csv}
		"""