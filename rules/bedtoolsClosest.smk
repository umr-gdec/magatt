rule selectMappedISBP:
	message: " Select only mapped ISBPS on new refseq"
	input: mapped=config['results']+'/1.filteredISBPs/{chrom}/sorted.bed',
		 original=config['isbpBed']
	output: config['results']+'/2.mappedISBPs/{chrom}/coordsOnQuery.bed'
	log: config['results']+'/2.mappedISBPs/{chrom}/coordsOnQuery.log'
	shell:
		"""
		cut -f 4 {input.mapped}|fgrep -wf - {input.original} |sort -k1,1 -k2,2n 1> {output} 2> {log}
		fgrep -i chrun {input.original} |sort -k1,1 -k2,2n 1>> {output} 2>> {log}
		"""

rule keepMappedOnSameChrom:
	message: " Select Mapped ISBPs on same chromosome (or on unknown chromosome)"
	input: isbpOnQuery=config['results']+'/2.mappedISBPs/{chrom}/coordsOnQuery.bed', 
		isbpOnTarget=config['results']+'/1.filteredISBPs/{chrom}/sorted.bed',
	output: config['results']+'/2.mappedISBPs/{chrom}/OnSameChrom.bed'
	log: config['results']+'/2.mappedISBPs/{chrom}/OnSameChrom.log'
	shell:
		"""
		cut -f 4 {input.isbpOnTarget}|fgrep -wf - {input.isbpOnQuery} |cut -f -4 |sort -k1,1 -k2,2n 1> {output} 2> {log}
		"""

rule upstreamClosest:
	message: " Collect closest marker upstream of genes"
	# container: "docker://helrim/magatt"
	input: annot=config['results']+"/1.features/{chrom}.bed", 
		markers=config['results']+'/2.mappedISBPs/{chrom}/OnSameChrom.bed'
	output: config['results']+'/2.closestbed/{chrom}/upstream.txt'
	log: config['results']+"/2.closestbed/{chrom}/upstream.log"
	shell:
		"""
		bedtools closest -k 5 -id -D ref -io -a {input.annot} -b {input.markers} 1> {output} 2> {log}
		"""

rule downstreamClosest:
	message: " Collect Downstream marker downstream of genes"
	# container: "docker://helrim/magatt"
	input: annot=config['results']+"/1.features/{chrom}.bed", 
			markers=config['results']+'/2.mappedISBPs/{chrom}/OnSameChrom.bed'
	output: config['results']+'/2.closestbed/{chrom}/downstream.txt'
	log: config['results']+"/2.closestbed/{chrom}/downstream.log"
	shell:
		"""
		bedtools closest -k 5 -iu -D ref -io -a {input.annot} -b {input.markers} 1> {output} 2> {log}
		"""
