rule isbpFastaFromBed:
	message: 'Extract ISBPs sequence from reference genome'
	# container: "docker://helrim/magatt:latest"
	#container: "docker://biocontainers/bedtools:v2.27.1dfsg-4-deb_cv1"
	input: bed=config['isbpBed'], fasta=config['queryFasta'], fai=config['queryFasta']+'.fai'
	output: config['results']+'/1.ISBPs.fasta'
	log: config['results']+'/1.ISBPS_fastafrombed.log'
	shell: "bedtools getfasta -name -fi {input.fasta} -bed {input.bed} -fo {output}"

rule isbpBwa:
	message: 'Map ISBPs on target genome'
	# container: "docker://helrim/magatt:latest"
	#container: "docker://dukegcb/bwa-samtools"
	input: fasta=config['results']+'/1.ISBPs.fasta'
	output: config['results']+'/1.MappedISBPs.bam'
	params: mapq=config['mapq'],F=config['flag_F'], bwaidx=config['targetBwaIdx']
	threads: config['bwaThreads']
	log: config['results']+'/1.MappedISBPs.log'
	shell: "bwa mem -t {threads} {params.bwaidx} {input.fasta} | samtools view -bS -F {params.F} -q {params.mapq} - | samtools sort -o {output}"

rule filterBam:
	message: "Filtering BAM file of ISBPs"
	# container: "docker://helrim/magatt:latest"
	input: config['results']+'/1.MappedISBPs.bam'
	output: config['results']+'/1.filteredISBPs.bam'
	params: mapq=config['mapq'], mismatches=config['mismatches']
	log: config['results']+'/1.filterBam.log'
	shell: "bin/filterBam.py {input} {output} {params.mapq} {params.mismatches} &> {log}"

rule bam2bed:
	message: "Convert Filtered BAM file into BED"
	# container: "docker://helrim/magatt:latest"
	#container: "docker://biocontainers/bedtools:v2.27.1dfsg-4-deb_cv1"
	input: config['results']+"/1.filteredISBPs.bam"
	output: config['results']+"/1.filteredISBPs/{chrom}/sorted.bed"
	log: config['results']+"/1.filteredISBPs/{chrom}/sorted.log"
	params: '{chrom}'
	shell: "bamToBed -i {input} |fgrep -i {params}|cut -d ':' -f 1|sort -k1,1 -k2,2n  1> {output} 2> {log}"

